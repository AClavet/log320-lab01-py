'''
Created on 2012-01-04

@author: Aleksandre Clavet
'''

import sys
from operator import itemgetter

NIL = "000"

class node:
    
    def __init__(self, caractere=None, frequence=0, gauche=None, droite=None, parent=None):
        self.caractere = caractere
        self.frequence = frequence
        self.parent = None
        #Utilise pour reconstruire le code equivalant dans le cas d'une feuille
        self.valeur = ""
        
        self.gauche = gauche
        if not self.gauche == None :
            gauche.valeur = "0"
            gauche.parent = self
            
        self.droite = droite
        if not self.droite == None :
            droite.valeur = "1"
            droite.parent = self
        
    def __gt__(self, other):
        return other.frequence < self.frequence
    
    def __ge__(self, other):
        return not self.frequence < other.frequence
    
    def __le__(self, other):
        return not other.frequence < self.frequence
    
    #http://en.wikipedia.org/wiki/Binary_search_tree
    def traverser(self, node, callback):
        if node is None:
            return
        self.traverser(node.gauche, callback)
        callback(node)
        self.traverser(node.droite, callback)  
    
class huffman :
    
    def _callback(self, node):
        print node.caractere, node.frequence
    
    def __init__(self):
        pass
    
    def compresser(self, src):
        #Charger le contenu du fichier
        contenu_fichier = self._lire_fichier(src)
        
        #Extraire la frequence des caracteres
        table_frequence = self._creer_table_frequence(contenu_fichier)
        
        #Ordonner les caracteres par ordre decroissant des frequences
        table_frequence_decroissant = self.custom_sort(table_frequence)
        
        arbre, feuilles = self._compresser(table_frequence_decroissant)
        
        codes = {} 
        code = ""
        for feuille in feuilles:
            code = feuille.valeur
            
            parent = feuille.parent
            while parent != None:
                code += parent.valeur
                parent = parent.parent
            codes[feuille.caractere] = code
        
        self._ecrire_representation_compressee("out.huf", contenu_fichier, codes)
    
    def decompresser(self, src):
        self._lire_fichier_compresse(src)
            
    def _lire_fichier(self, src):
        donnees = []
        with open(src, "r") as f:
            octal_byte = f.read(3)
            donnees.append(octal_byte)
            while octal_byte != "":
                donnees.append(octal_byte)
                octal_byte = f.read(3)
        return donnees
    
    def _lire_fichier_compresse(self, src):
        donnees = []
        contenu = open(src).read().split('|')
        
        print "frequences" + contenu[0]
        print "message" + contenu[1]
        
        return donnees
        
    def _creer_table_frequence(self, contenu):
        table_frequence = {}
        
        for entree in contenu :
            if entree in table_frequence.keys():
                table_frequence[entree] += 1
            else :
                table_frequence[entree] = 1
        return table_frequence 
                
    def custom_sort(self, table_frequence):
        return sorted(table_frequence.items(), key=itemgetter(1))
    
    def _compresser(self, table_frequence):
        branches = []
        feuilles = []
        
        for c in table_frequence:
            branches.append(node(c[0], c[1]))
            
        branches.sort(reverse=True)    
        
        nil_node = None
        while len(branches) > 1:
            b1, b2 = branches.pop() , branches.pop()
            frequence = b1.frequence + b2.frequence
            
            nil_node = node("*", frequence, b1, b2)
            
            branches.append(nil_node)
  
            if b1.gauche == None and b1.droite == None :
                feuilles.append(b1)
            
            if b2.gauche == None and b2.droite == None:
                feuilles.append(b2)
                
            branches.sort(reverse=True)
        return nil_node, feuilles
    
    
    def _ecrire_representation_compressee(self, fichier_compresse, contenu, codes):
        f = file(fichier_compresse, 'w')
        for c in codes : 
            f.write(c + " " + codes[c] + "\n")
           
        f.write("|\n")
            
        for c in contenu : 
            print "contenu original " + c, "nouvelle valeur : " + codes[c]
            f.write(codes[c] + "\n")
        f.close()
        
    
if __name__ == '__main__':
    #HUFFMAN = huffman(sys.argv[1])
    HUFFMAN = huffman()
    HUFFMAN.compresser("test.huf")
    #HUFFMAN.decompresser("out.huf")
